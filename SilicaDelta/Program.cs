﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SilicaDelta
{
    class Program
    {

        public static void CreatePatch(Stream infile, Stream patchedFile, Stream outfile)
        {
            BinaryWriter boutfile = new BinaryWriter(outfile);
            for (int i = 0; i < infile.Length; i++)
            {
                long pos = infile.Position;
                byte infileByte = (byte)infile.ReadByte();
                byte patchedfileByte = (byte)patchedFile.ReadByte();

                if (infileByte != patchedfileByte)
                {
                    boutfile.Write((UInt64)pos);
                    outfile.WriteByte(patchedfileByte);
                }
            }
        }

        public static void ApplyPatch(FileStream infile, FileStream patchedFile, FileStream outfile)
        {
            infile.CopyTo(outfile);
            BinaryReader bpatch = new BinaryReader(patchedFile);

            while(patchedFile.Position < patchedFile.Length)
            {
                Int64 offset = bpatch.ReadInt64();
                outfile.Seek(offset, SeekOrigin.Begin);
                outfile.WriteByte((byte)patchedFile.ReadByte());
            }
            

        }
        static void Main(string[] args)
        {
            if(args.Length != 4)
            {
                Console.WriteLine("SilicaDelta Usage: <mode: create/patch> <infile> <patchedfile> <output>");
                return;
            }

            FileStream infile = new FileStream(args[1], FileMode.OpenOrCreate, FileAccess.ReadWrite);
            FileStream patchedFile = new FileStream(args[2], FileMode.OpenOrCreate, FileAccess.ReadWrite);
            FileStream outfile = new FileStream(args[3], FileMode.CreateNew, FileAccess.ReadWrite);
            if (args[0].ToLower() == "create")
            {
                CreatePatch(infile, patchedFile, outfile);
            }
            else if(args[0].ToLower() == "patch")
            {
                ApplyPatch(infile, patchedFile, outfile);
            }
            else
            {
                Console.WriteLine("Invalid mode! valid options are create or patch");
                return;
            }

        }
    }
}
